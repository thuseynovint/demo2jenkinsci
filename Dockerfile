FROM maven:3.6.3-jdk-8
ARG JAR_FILE=target/*.jar
WORKDIR /app
COPY src /app/src
COPY pom.xml /app
RUN mvn clean package -DskipTest
RUN mv ${JAR_FILE} demo2.jar
ENV SPRING_PROFILES_ACTIVE=mysql
ENV SPRING_DATASOURCE_URL=jdbc:mysql://mysql/petclinic
ENV SPRING_DATASOURCE_USERNAME=petclinic
ENV SPRING_DATASOURCE_PASSWORD=petclinic
CMD ["java", "-jar", "demo2.jar"]





